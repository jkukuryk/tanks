export const ZINDEX = {
  background: 1,
  player: 100,
  food: 101,
  beforePlayer: 1000,
  layout: 9999,
};
