import { ActionType } from "./actionTypes";

export type GameClass = {
  Start?: () => void;
  Reset?: () => void;
  Preload?: () => void;
  Action?: (type: ActionType, data: any) => void;
};

export enum GameScene {
  START = "START",
  PLAY = "PLAY",
}

export enum CellType {
  WALL = "wall",
  HAY = "hay",
  GAME_BORDER = "game_border",
}
export type Position = {
  x: number;
  y: number;
};
export type Dimensions = {
  width: number;
  height: number;
};
