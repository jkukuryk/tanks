import mainTheme from "sounds/mainTheme.mp3";
import diamond from "sounds/fire.mp3";
import collisionWall from "sounds/collisionWall.mp3";
import collisionHay from "sounds/collisionHay.mp3";

export enum SoundsType {
  MAIN_THEME = "mainTheme",
  FIRE = "fire",
  COLLISION_WALL = "collisionWall",
  COLLISION_HAY = "collisionHay",
}

export const Sounds = {
  [SoundsType.MAIN_THEME]: mainTheme,
  [SoundsType.FIRE]: diamond,
  [SoundsType.COLLISION_WALL]: collisionWall,
  [SoundsType.COLLISION_HAY]: collisionHay,
};
