export const CELL_WIDTH = 35;
export const CELL_HEIGHT = 35;
export const CELLS_HORIZONTAL = 50;
export const CELLS_VERTICAL = 50;

export const GAME_WIDTH = CELL_WIDTH * CELLS_HORIZONTAL;
export const GAME_HEIGHT = CELL_HEIGHT * CELLS_VERTICAL;
