import "reset-css";
import { start, GameInstance } from "classes/gameEngine";
import { Ground } from "src/stages/ground";
import { Tanks } from "src/stages/tanks/tanks";
import { Level } from "src/stages/level";
import { Bullets } from "src/stages/bullets";
import { Particles } from "src/stages/particles";
import { SplashScreen } from "src/stages/splashScreen";
import { Overlay } from "src/stages/overlay";
import { Score } from "src/stages/score";

GameInstance.RegisterStages({
  ground: new Ground(),
  tanks: new Tanks(),
  level: new Level(),
  bullets: new Bullets(),
  particles: new Particles(),
  splashScreen: new SplashScreen(),
  overlay: new Overlay(),
  score: new Score(),
});

start();
