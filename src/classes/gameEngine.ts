import * as PIXI from "pixi.js";
import {
  Dimensions,
  GameClass,
  GameScene,
  Position,
} from "constants/globalTypes";
import { imageManager } from "classes/imageManager";
import { inputHandler } from "classes/inputClass";
import { playSound, toggleMute } from "classes/soundManager";
import { ActionType } from "src/constants/actionTypes";
import { GAME_WIDTH, GAME_HEIGHT } from "constants/dimensions";
import { resetColliders } from "./colliders";
import { SoundsType } from "src/constants/sounds";
const REALOD_GAME_DELAY = 2 * 1000;

class Game {
  app: PIXI.Application;
  stages: { [name: string]: GameClass };
  gameSize: Dimensions;
  viewTransition: Position;
  scene: GameScene;
  stagesList: string[];
  gameContainer: PIXI.Container;
  mainThemeAdded: boolean;
  resetDelay: number;

  constructor() {
    this.scene = GameScene.START;
    this.resetDelay = Date.now() + REALOD_GAME_DELAY;
    this.CenterView = this.CenterView.bind(this);
  }

  public Start() {
    this.app = new PIXI.Application({
      width: 1,
      height: 1,
    });
    this.gameContainer = new PIXI.Container();
    this.app.stage.addChild(this.gameContainer);
    this.gameSize = { width: GAME_WIDTH, height: GAME_HEIGHT };
    this.viewTransition = { x: 0, y: 0 };
    document.getElementById("game_container").innerHTML = "";
    document.getElementById("game_container").appendChild(this.app.view);
    this.CenterView();

    this.stagesList.forEach((stageName: string) => {
      const stageClass = this.stages[stageName];

      if (stageClass.Preload) {
        stageClass.Preload();
      }
    });

    this._startGame();
    this._setViewTransitionTicker();
  }
  private _startGame() {
    resetColliders();
    this.stagesList.forEach((stageName: string) => {
      const stageClass = this.stages[stageName];
      if (stageClass.Start) {
        stageClass.Start();
      }
    });
    this.resetDelay = Date.now() + REALOD_GAME_DELAY;
  }
  public canPlay() {
    return this.resetDelay === 0;
  }
  private _resetGame() {
    this.gameContainer.removeChildren();
    this.stagesList.forEach((stageName: string) => {
      const stageClass = this.stages[stageName];
      if (stageClass.Reset) {
        stageClass.Reset();
      }
    });
    this._startGame();
  }
  public Emit(type: ActionType, input?: { [key: string]: any } | null) {
    this.stagesList.forEach((stageName: string) => {
      const stageClass = this.stages[stageName];
      if (stageClass.Action) {
        stageClass.Action(type, input);
      }
    });
    this._action(type, input);
  }

  private _action(type: ActionType, input: any) {
    switch (type) {
      case ActionType.KEYDOWN:
        if (input.keyCode === "KeyR") {
          this._resetGame();
        } else {
          if (this.resetDelay !== 0) {
            if (
              (this.resetDelay < Date.now() && this.resetDelay !== 0) ||
              input.keyCode === "Enter"
            ) {
              this.resetDelay = 0;
              this.Emit(ActionType.START_GAME_PLAY);
            }
          }
        }
        if (input.keyCode === "KeyS") {
          toggleMute();
        }
        if (!this.mainThemeAdded) {
          this.mainThemeAdded = true;
          playSound(SoundsType.MAIN_THEME, "infinity");
        }

        break;
      case ActionType.GAME_OVER:
        this._resetGame();
        break;
    }
  }
  public CenterView() {
    this.gameSize = { width: GAME_WIDTH, height: GAME_HEIGHT };

    const minSizeW = Math.min(window.innerWidth, GAME_WIDTH);
    const minSizeH = Math.min(window.innerHeight, GAME_HEIGHT);

    this.gameSize = { width: minSizeW, height: minSizeH };
    if (this.app) {
      this.app.renderer.resize(minSizeW, minSizeH);
      this.changeViewTransition(this.viewTransition);
    }
  }

  public RegisterStages(stages: { [name: string]: GameClass }) {
    this.stages = stages;
    this.stagesList = Object.keys(this.stages);
  }

  public addContainer() {
    const container = new PIXI.Container();
    this.gameContainer.addChild(container);
    return container;
  }

  public changeViewTransition(position: Position) {
    const { x, y } = position;
    let newX = x - this.gameSize.width / 2;
    let newY = y - this.gameSize.height / 2;

    if (newY < 0) {
      newY = 0;
    }
    if (y + this.gameSize.height / 2 > GAME_HEIGHT) {
      newY = GAME_HEIGHT - this.gameSize.height;
    }
    if (newX < 0) {
      newX = 0;
    }

    if (x + this.gameSize.width / 2 > GAME_WIDTH) {
      newX = GAME_WIDTH - this.gameSize.width;
    }
    this.viewTransition = { x: newX * -1, y: newY * -1 };
  }

  private _setViewTransitionTicker() {
    this.app.ticker.add(() => {
      let currentXTransition = this.gameContainer.position.x;
      let currentYTransition = this.gameContainer.position.y;

      if (currentXTransition !== this.viewTransition.x) {
        const diffX = currentXTransition - this.viewTransition.x;
        if (Math.abs(diffX) < 1) {
          currentXTransition = this.viewTransition.x;
        } else {
          currentXTransition = currentXTransition - diffX / 3;
        }
      }

      if (currentYTransition !== this.viewTransition.y) {
        const diffY = currentYTransition - this.viewTransition.y;
        if (Math.abs(diffY) < 1) {
          currentYTransition = this.viewTransition.y;
        } else {
          currentYTransition = currentYTransition - diffY / 3;
        }
      }

      this.gameContainer.position.x = Math.round(currentXTransition);
      this.gameContainer.position.y = Math.round(currentYTransition);
    });
  }
}

export const GameInstance = new Game();

export function start() {
  window.addEventListener("resize", GameInstance.CenterView, true);
  window.addEventListener("keydown", inputHandler.keyDown, true);
  window.addEventListener("keyup", inputHandler.keyUp, true);

  imageManager.loadImages(() => GameInstance.Start());
}
