import * as PIXI from "pixi.js";
import { CELL_HEIGHT, CELL_WIDTH } from "src/constants/dimensions";

export type DrawRect = {
  x: number;
  y: number;
  width: number;
  height: number;
};

export type DrawImageInput = {
  x?: number;
  y?: number;
  rotation?: number;
};

type ImageList = {
  [key: string]: {
    isLoaded: boolean;
    src: string;
    name: string;
    isRendered: boolean;
    sprite: PIXI.Sprite | null;
  };
};

class ImageManager {
  images: ImageList;
  isLoading: boolean;

  constructor() {
    this.images = {} as ImageList;
    this.loadImages = this.loadImages.bind(this);
  }

  public loadImages(callback: () => void) {
    const loader = new PIXI.Loader();

    Object.keys(this.images).forEach((imageAlias: string) => {
      const image = this.images[imageAlias];
      loader.add(image.name, image.src);
    });
    loader.onComplete.add(() => callback());
    loader.load((loader: PIXI.Loader) => {
      Object.keys(loader.resources).forEach((imageAlias: string) => {
        const sprite = new PIXI.Sprite(loader.resources[imageAlias].texture);
        this.images[imageAlias].sprite = sprite;
      });
    });
  }

  public add(name: string, src: string) {
    if (!this.images[name]) {
      this.images[name] = {
        isLoaded: false,
        src,
        name,
        sprite: null,
        isRendered: false,
      };
    }
  }

  public get(alias: string) {
    if (this.images[alias]) {
      return this.images[alias];
    }
    return null;
  }

  public getTexture(alias: string) {
    const img = this.get(alias);
    if (img) {
      return PIXI.Texture.from(img.src);
    }
    return null;
  }

  public addTileSpriteToContainer(
    spriteName: string,
    container: PIXI.Container,
    position: DrawImageInput = { x: 0, y: 0 },
    rect: DrawRect = { x: 0, y: 0, width: CELL_WIDTH, height: CELL_HEIGHT }
  ) {
    const texture = this.getTexture(spriteName);
    if (texture) {
      const currentSprite = new PIXI.TilingSprite(
        texture,
        rect.width,
        rect.height
      );

      container.addChild(currentSprite);
      currentSprite.position.set(position.x, position.y);
      currentSprite.tilePosition.x = rect.x * rect.width * -1;
      currentSprite.tilePosition.y = rect.y * rect.height * -1;

      return currentSprite;
    }
    return null;
  }

  public addSpriteToContainer(spriteName: string, container: PIXI.Container) {
    const texture = this.getTexture(spriteName);
    if (texture) {
      const currentSprite = new PIXI.Sprite(texture);

      container.addChild(currentSprite);
      return currentSprite;
    }
    return null;
  }
}

export const imageManager = new ImageManager();
