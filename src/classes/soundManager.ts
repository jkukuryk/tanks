import { Sounds, SoundsType } from "src/constants/sounds";

type SoundKey = { [key: string]: HTMLAudioElement };
type Loops = {
  [key: string]: { loop: number | "infinity" };
};
const MAX_VOLUME = 0.5;

class SoundManager {
  sounds: SoundKey;
  imagesSrc: string[];
  imageLoaded: string[];
  imageLoading: string[];
  initialLoad: string[];
  loops: Loops;
  muted: boolean;

  constructor() {
    this.sounds = {} as SoundKey;
    this.addSound = this.addSound.bind(this);
    this.playInLoop = this.playInLoop.bind(this);
    this.loops = {} as Loops;
    this.muted = false;
  }

  public toggleMute() {
    this.muted = !this.muted;
    Object.keys(this.sounds).forEach((src: SoundsType) => {
      const audio = this.sounds[src];
      if (this.muted) {
        audio.volume = 0;
      } else {
        audio.volume = MAX_VOLUME;
      }
    });
  }
  public isMuted() {
    return this.muted;
  }

  public addSound(key: SoundsType, audio: HTMLAudioElement): void {
    this.sounds[key] = audio;
  }

  public play(src: SoundsType) {
    const audio = new Audio();
    audio.src = Sounds[src];
    this.addSound(src, audio);

    audio.oncanplay = () => {
      if (!manager.isMuted()) {
        audio.volume = MAX_VOLUME;
        audio.play();
      }
    };
  }

  public playInLoop(src: SoundsType, loops: number | "infinity") {
    const self = this;
    const audio = new Audio();
    audio.src = Sounds[src];
    this.addSound(src, audio);

    audio.oncanplay = () => {
      if (!manager.isMuted()) {
        audio.volume = MAX_VOLUME;
      }
      audio.play();

      this.loops[src] = { loop: loops };
    };

    audio.onended = function () {
      let newLoop = self.loops[src].loop;

      if (newLoop > 0 || newLoop === "infinity") {
        if (newLoop !== "infinity") {
          newLoop = newLoop--;
        }
        self.loops[src] = { loop: newLoop };
        self.playInLoop(src, newLoop);
      }
    };
  }
}

const manager = new SoundManager();

export function playSound(src: SoundsType, loops?: number | "infinity") {
  if (!manager.isMuted()) {
    if (loops === 0) {
      manager.play(src);
    } else {
      manager.playInLoop(src, loops);
    }
  }
}
export function toggleMute() {
  manager.toggleMute();
}
