import { GameInstance } from "./gameEngine";
import { ActionType } from "constants/actionTypes";

const LEFT_CODES = ["ArrowLeft", "KeyA"];
const RIGHT_CODES = ["ArrowRight", "KeyD"];
const TOP_CODES = ["ArrowUp", "KeyW"];
const BOTTOM_CODES = ["ArrowBottom", "KeyS"];

class InputClass {
  pressedKeys: string[];

  constructor() {
    this.pressedKeys = [];
    this.keyUp = this.keyUp.bind(this);
    this.keyDown = this.keyDown.bind(this);
  }
  public keyUp(e: KeyboardEvent) {
    const keyCode = e.code;
    this.removeKeys([keyCode]);
  }

  public keyDown(e: KeyboardEvent) {
    const keyCode = e.code;
    this.addKey(keyCode);
    GameInstance.Emit(ActionType.KEYDOWN, { keyCode });
  }

  private addKey(code: string) {
    this.pressedKeys.push(code);
  }

  private removeKeys(keyCodes: string[]) {
    this.pressedKeys = this.pressedKeys.filter(
      (keyCode) => !keyCodes.includes(keyCode)
    );
  }

  public getHorizontalInput() {
    const leftIndex = this.pressedKeys.findIndex((key) =>
      LEFT_CODES.includes(key)
    );
    const rightIndex = this.pressedKeys.findIndex((key) =>
      RIGHT_CODES.includes(key)
    );
    if (leftIndex === -1 && rightIndex === -1) {
      return 0;
    }
    return leftIndex > rightIndex ? -1 : 1;
  }
  public getVerticalInput() {
    const topIndex = this.pressedKeys.findIndex((key) =>
      TOP_CODES.includes(key)
    );
    const bottomIndex = this.pressedKeys.findIndex((key) =>
      BOTTOM_CODES.includes(key)
    );
    if (topIndex === -1 && bottomIndex === -1) {
      return 0;
    }
    return topIndex > bottomIndex ? 1 : -1;
  }
}
export const inputHandler = new InputClass();

export const getHorizontalInput = () => {
  return inputHandler.getHorizontalInput();
};

export const getVerticalInput = () => {
  return inputHandler.getVerticalInput();
};
