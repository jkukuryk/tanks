import { CellType, Dimensions, Position } from "src/constants/globalTypes";
import { CELL_HEIGHT, CELL_WIDTH } from "src/constants/dimensions";

export type Collider = {
  x: number;
  y: number;
  width: number;
  height: number;
  type: CellType;
  id: string;
  container?: PIXI.Container;
};
class CollidersClass {
  colliders: Collider[];
  nextId: number;
  constructor() {
    this.colliders = [] as Collider[];
    this.nextId = 1;
  }

  findCollider(
    newPosition: Position,
    cellSize = { width: CELL_WIDTH, height: CELL_HEIGHT }
  ) {
    const collider = this.colliders.find((collider) => {
      const { x, y, width, height } = collider;
      if (newPosition.x + cellSize.width > x && newPosition.x < x + width) {
        if (newPosition.y + cellSize.height > y && newPosition.y < y + height) {
          return true;
        }
      }
      return false;
    });

    return collider || null;
  }

  removeCollider(id: string) {
    this.colliders = [
      ...this.colliders.filter((collider) => collider.id !== id),
    ];
  }
  resetColliders() {
    this.colliders = [] as Collider[];
    this.nextId = 1;
  }

  addCollider(
    position: Position,
    type: CellType,
    size = { width: CELL_WIDTH, height: CELL_HEIGHT }
  ) {
    this.nextId++;
    const newId = `collider_${type}_${this.nextId}`;
    this.colliders.push({
      ...position,
      id: newId,
      type,
      width: size.width,
      height: size.height,
    });
    return newId;
  }
}
export const colliders = new CollidersClass();

export const getCollider = (
  targetPosition: Position,
  cellSize?: Dimensions
) => {
  return colliders.findCollider(targetPosition, cellSize);
};

export const addCollider = (
  position: Position,
  type: CellType,
  size?: Dimensions
) => {
  return colliders.addCollider(position, type, size);
};

export const removeCollider = (id: string) => {
  return colliders.removeCollider(id);
};

export const resetColliders = () => {
  return colliders.resetColliders();
};
