import { imageManager } from "classes/imageManager";
import overlay from "assets/overlay.png";

import { GameInstance } from "src/classes/gameEngine";
import { ActionType } from "src/constants/actionTypes";
import * as PIXI from "pixi.js";

export class Overlay {
  overlayContainer: PIXI.Container;
  opacity: number;
  show: boolean;

  constructor() {
    imageManager.add("overlay", overlay);
    this.overlayContainer = new PIXI.Container();
    this.show = false;
    this.opacity = 0;
  }

  public Preload() {
    GameInstance.app.stage.addChild(this.overlayContainer);
    imageManager.addSpriteToContainer("overlay", this.overlayContainer);
    GameInstance.app.ticker.add(() => {
      if (this.show && this.opacity < 0.5) {
        this.opacity += 0.1;
      }
      if (!this.show && this.opacity > 0) {
        this.opacity -= 0.05;
      }
      this.overlayContainer.alpha = this.opacity;
    });
  }

  public Action(action: ActionType, data: any) {
    switch (action) {
      case ActionType.AFTER_LEVEL_BUILD:
        this.show = false;
        break;

      case ActionType.START_GAME_PLAY:
        this.show = true;
        break;
    }
  }
}
