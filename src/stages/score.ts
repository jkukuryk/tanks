import { imageManager } from "classes/imageManager";
import numbers from "assets/numbers.png";
import hayPointer from "assets/hayPointer.png";

import { GameInstance } from "src/classes/gameEngine";
import { ActionType } from "src/constants/actionTypes";
import * as PIXI from "pixi.js";
import { CELL_HEIGHT, CELL_WIDTH } from "src/constants/dimensions";
const SCORE_MARGIN = 5;
const ONE_SECOUND = 1000;
const GAME_TIME = ONE_SECOUND * 60; //1 minut;

export class Score {
  pointerContainer: PIXI.Container;
  unitContainer: PIXI.Container;
  tensContainer: PIXI.Container;
  unitSprite: PIXI.TilingSprite;
  tensSprite: PIXI.TilingSprite;

  unitTimeContainer: PIXI.Container;
  tensTimeContainer: PIXI.Container;
  unitTimeSprite: PIXI.TilingSprite;
  tensTimeSprite: PIXI.TilingSprite;

  score: number;
  startGame: number;
  blink: boolean;
  alpha: number;
  timer: number;

  constructor() {
    imageManager.add("numbers", numbers);
    imageManager.add("hayPointer", hayPointer);
    this.pointerContainer = new PIXI.Container();
    this.unitContainer = new PIXI.Container();
    this.tensContainer = new PIXI.Container();
    this.unitTimeContainer = new PIXI.Container();
    this.tensTimeContainer = new PIXI.Container();

    this.score = 0;
    this.startGame = 0;
    this.alpha = 0;
    this.timer = 0;
    this.blink = true;
  }

  public Preload() {
    GameInstance.app.stage.addChild(this.pointerContainer);
    GameInstance.app.stage.addChild(this.unitContainer);
    GameInstance.app.stage.addChild(this.tensContainer);
    GameInstance.app.stage.addChild(this.unitTimeContainer);
    GameInstance.app.stage.addChild(this.tensTimeContainer);

    this.pointerContainer.alpha = this.alpha;
    this.unitContainer.alpha = this.alpha;
    this.tensContainer.alpha = this.alpha;
    this.unitTimeContainer.alpha = this.alpha;
    this.tensTimeContainer.alpha = this.alpha;

    this.unitSprite = this._addSprite(this.unitContainer);
    this.tensSprite = this._addSprite(this.tensContainer);
    this.unitTimeSprite = this._addSprite(this.unitTimeContainer);
    this.tensTimeSprite = this._addSprite(this.tensTimeContainer);

    imageManager.addSpriteToContainer("hayPointer", this.pointerContainer);
    this._repositionScore();

    GameInstance.app.ticker.add(() => {
      this._repositionScore();

      if (this.startGame + GAME_TIME < Date.now() && this.startGame !== 0) {
        this.startGame = 0;
        this.blink = true;
        this.alpha = 0.5;
        GameInstance.Emit(ActionType.GAME_OVER);
      }

      if (this.startGame === 0) {
        if (this.blink && this.alpha < 1) {
          this.alpha += 0.1;
          if (this.alpha >= 1) {
            this.blink = false;
            this.alpha = 1;
          }
        }
        if (!this.blink && this.alpha > 0) {
          this.alpha -= 0.1;
          if (this.alpha <= 0) {
            this.blink = true;
            this.alpha = 0;
          }
        }
      }

      this.pointerContainer.alpha = this.alpha;
      this.unitContainer.alpha = this.alpha;
      this.tensContainer.alpha = this.alpha;
      this.tensTimeContainer.alpha = this.alpha;
      this.unitTimeContainer.alpha = this.alpha;
      if (this.startGame !== 0) {
        this.timer =
          GAME_TIME / ONE_SECOUND -
          Math.floor(Math.abs(this.startGame - Date.now()) / ONE_SECOUND);
      }
    });
  }
  Start() {
    this.timer = 0;
  }

  _addSprite(container: PIXI.Container) {
    const sprite = imageManager.addTileSpriteToContainer(
      "numbers",
      container,
      {
        x: 0,
        y: 0,
      },
      {
        x: 0,
        y: 0,
        width: CELL_WIDTH,
        height: CELL_HEIGHT,
      }
    );
    return sprite;
  }

  _repositionScore() {
    this.pointerContainer.position.set(
      GameInstance.gameSize.width - CELL_WIDTH - SCORE_MARGIN,
      SCORE_MARGIN
    );
    this.unitContainer.position.set(
      GameInstance.gameSize.width - CELL_WIDTH * 2 - SCORE_MARGIN,
      SCORE_MARGIN
    );
    this.tensContainer.position.set(
      GameInstance.gameSize.width - CELL_WIDTH * 3 - SCORE_MARGIN,
      SCORE_MARGIN
    );
    this.unitTimeContainer.position.set(
      GameInstance.gameSize.width / 2 - CELL_WIDTH + 12,
      SCORE_MARGIN
    );
    this.tensTimeContainer.position.set(
      GameInstance.gameSize.width / 2 + CELL_WIDTH - 12,
      SCORE_MARGIN
    );

    const tens = Math.floor(this.score / 10);
    const unit = this.score % 10;
    this.unitSprite.tilePosition.set(unit * CELL_WIDTH * -1, 0);
    this.tensSprite.tilePosition.set(tens * CELL_WIDTH * -1, 0);

    const tensTime = Math.floor(this.timer / 10);
    const unitTime = this.timer % 10;
    this.unitTimeSprite.tilePosition.set(tensTime * CELL_WIDTH * -1, 0);
    this.tensTimeSprite.tilePosition.set(unitTime * CELL_WIDTH * -1, 0);
  }

  _addScore() {
    this.score++;
  }
  Reset() {
    this.timer = 0;
    this.startGame = 0;
  }
  public Action(action: ActionType, data: any) {
    switch (action) {
      case ActionType.HAY_DESTROYED:
        this._addScore();
        break;
      case ActionType.START_GAME_PLAY:
        this.score = 0;
        this.startGame = Date.now();
        this.alpha = 0.8;
        this.timer = 0;

        break;
    }
  }
}
