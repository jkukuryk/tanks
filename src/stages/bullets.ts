import * as PIXI from "pixi.js";

import { imageManager } from "src/classes/imageManager";

import { CellType, Position } from "src/constants/globalTypes";
import mainSprite from "assets/tanksSprite.png";
import { ActionType } from "src/constants/actionTypes";

import { Tank, TankColor } from "./tanks/tanks";
import { Directions } from "./tanks/frames";
import { GameInstance } from "src/classes/gameEngine";
import { CELL_HEIGHT, CELL_WIDTH } from "src/constants/dimensions";
import { getCollider } from "src/classes/colliders";
import { SoundsType } from "src/constants/sounds";
import { playSound } from "src/classes/soundManager";
const SPEED = 10;
export const BULLET_SIZE = 11;

interface Bullet {
  damege: number;
  addMove: { x: number; y: number };
  container: PIXI.Container;
}

const bulletDamage = {
  [TankColor.RED]: 10,
  [TankColor.BLUE]: 20,
  [TankColor.GREEN]: 25,
};

export class Bullets {
  bullets: Bullet[];

  constructor() {
    imageManager.add("mainSprite", mainSprite);
    this.Reset();
  }

  public Reset() {
    this.bullets = [] as Bullet[];
  }

  Preload() {
    GameInstance.app.ticker.add(() => {
      this.bullets.forEach((bullet, key) => {
        const newPosition = {
          x: bullet.container.x + bullet.addMove.x * SPEED,
          y: bullet.container.y + bullet.addMove.y * SPEED,
        };
        const collision = getCollider(newPosition, {
          width: BULLET_SIZE,
          height: BULLET_SIZE,
        });
        if (collision) {
          this.bullets.splice(key, 1);
          bullet.container.destroy();
          GameInstance.Emit(ActionType.HIT, {
            collider: collision,
            power: bullet.damege,
          });
          GameInstance.Emit(ActionType.ADD_SMOKE, {
            position: {
              x: newPosition.x + BULLET_SIZE / 2,
              y: newPosition.y + BULLET_SIZE / 2,
            },
          });

          if (collision.type === CellType.WALL) {
            playSound(SoundsType.COLLISION_WALL, 0);
          }
          if (collision.type === CellType.HAY) {
            playSound(SoundsType.COLLISION_HAY, 0);
            GameInstance.Emit(ActionType.ADD_HAY_DAMAGE, {
              position: {
                x: collision.x,
                y: collision.y,
              },
            });
          }
        } else {
          bullet.container.position.set(newPosition.x, newPosition.y);
        }
      });
    });
  }

  Action(action: ActionType, data: any) {
    switch (action) {
      case ActionType.FIRE:
        const bulletContainer = GameInstance.addContainer();

        const { color, direction } = data.tank as Tank;
        const bulletPosition = this._getPositionFromTank(data.tank);
        bulletContainer.position.set(bulletPosition.x, bulletPosition.y);
        const addMove = this._getMoveByDirection(direction);
        const newBullet = {
          damege: bulletDamage[color],
          addMove,
          container: bulletContainer,
        };

        imageManager.addTileSpriteToContainer(
          "mainSprite",
          bulletContainer,
          {
            x: 0,
            y: 0,
          },
          {
            x: 1,
            y: 5,
            width: CELL_WIDTH,
            height: CELL_HEIGHT,
          }
        );

        this.bullets.push(newBullet);
        GameInstance.Emit(ActionType.ADD_SMOKE, {
          position: {
            x: bulletPosition.x + BULLET_SIZE / 2,
            y: bulletPosition.y + BULLET_SIZE / 2,
          },
        });
        playSound(SoundsType.FIRE, 0);
        break;
    }
  }
  _getPositionFromTank(tank: Tank) {
    const { direction, container } = tank;
    const { position } = container;
    switch (direction) {
      case Directions.TOP:
        return {
          x: Math.floor(position.x + CELL_WIDTH / 2 - BULLET_SIZE / 2),
          y: position.y,
        };
      case Directions.LEFT:
        return {
          x: position.x,
          y: Math.floor(position.y + CELL_HEIGHT / 2 - BULLET_SIZE / 2),
        };
      case Directions.BOTTOM:
        return {
          x: Math.floor(position.x + CELL_WIDTH / 2 - BULLET_SIZE / 2),
          y: Math.floor(position.y + CELL_HEIGHT),
        };
      case Directions.RIGHT:
        return {
          x: Math.floor(position.x + CELL_WIDTH),
          y: Math.floor(position.y + CELL_HEIGHT / 2 - BULLET_SIZE / 2),
        };
    }
  }
  _getMoveByDirection(direction: Directions) {
    switch (direction) {
      case Directions.TOP:
        return { x: 0, y: -1 };
      case Directions.LEFT:
        return { x: -1, y: 0 };

      case Directions.BOTTOM:
        return { x: 0, y: 1 };

      case Directions.RIGHT:
        return { x: 1, y: 0 };
    }
  }
}
