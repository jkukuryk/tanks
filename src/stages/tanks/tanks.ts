/***
 * We have three tanks: Red, Blue, Green. They can fire bullets.
 * Tank can fire just in one direction - straightforward.
 * The damage of bullet for red = 10, blue = 20, Green = 25.
 * Red fires 2 bullets, Blue fires 3 bullets and Green fires 1 bullet.
 * You can move and rotate the tank using the keyboard, but can't go through walls and hays.
 * Players can change the tank anytime during gameplay by clicking button T on keyboard.
 ***/

import * as PIXI from "pixi.js";
import { imageManager } from "classes/imageManager";
import mainSprite from "assets/tanksSprite.png";
import activeIcon from "assets/activeIcon.png";
import { valuesIn } from "lodash";
import { CELL_HEIGHT, CELL_WIDTH } from "constants/dimensions";
import { GameInstance } from "src/classes/gameEngine";
import { FRAMES, Directions, AnimationType, ICON_FRAMES } from "./frames";
import { ActionType } from "src/constants/actionTypes";
import { getHorizontalInput, getVerticalInput } from "src/classes/inputClass";
import { Position } from "src/constants/globalTypes";
import { getCollider } from "src/classes/colliders";

const MAX_SPEED = 7;
const SLOW_DOWN = 0.5;
const ACCELERATION = 0.33;
const ROTATION_FRAMES = 3;
const ACTIVE_ICON_TICKER = 3;
const ANIMATION_SPEED_FRAME = 1;
const NEXT_ROTATION_DELAY = 10;
const ACTIVE_ICON_CELL_SIZE = 45;
const FIRE_DELAY = 6;
const RELOAD_TICKS = 15;

export enum TankColor {
  RED = "red",
  BLUE = "blue",
  GREEN = "green",
}

const bulletsCount = {
  [TankColor.RED]: 2,
  [TankColor.BLUE]: 3,
  [TankColor.GREEN]: 1,
};

const colorHue = {
  [TankColor.RED]: -100,
  [TankColor.BLUE]: 100,
  [TankColor.GREEN]: 0,
};

export interface Tank {
  color: TankColor;
  position: Position;
  direction: Directions;
  speed: number;
  container: PIXI.Container;
  sprite: PIXI.TilingSprite;
  ticker: number;
  rotationFrame: number;
  rotationTickBlock: number;
  prevDirection: Directions;
  fireDelay: number;
  fireReload: number;
  fire: number;
}

interface ActiveIcon {
  position: Position;
  container: PIXI.Container;
  sprite: PIXI.TilingSprite;
  ticker: number;
}

export class Tanks {
  activeTank: number;
  tanks: Tank[];
  activeIcon: ActiveIcon;
  ticker: PIXI.Ticker;

  constructor() {
    imageManager.add("mainSprite", mainSprite);
    imageManager.add("activeIcon", activeIcon);
    this.Reset();
  }

  public Reset() {
    this.tanks = [] as Tank[];
    this.activeTank = 0;
  }

  public Preload() {
    this._handleTankMove();
  }

  public Action(action: ActionType, data: any) {
    switch (action) {
      case ActionType.AFTER_LEVEL_BUILD:
        this._generateTanks();
        break;

      case ActionType.KEYDOWN:
        switch (data.keyCode) {
          case "KeyT":
            if (GameInstance.canPlay()) {
              this.activeTank++;
              if (this.activeTank >= this.tanks.length) {
                this.activeTank = 0;
              }
              GameInstance.changeViewTransition({
                x: this._getActiveTank().container.position.x,
                y: this._getActiveTank().container.position.y,
              });
            }
            break;
          case "Space":
          case "ArrowDown":
            if (GameInstance.canPlay()) {
              const tank = this._getActiveTank();
              if (tank && tank.fireReload === 0) {
                tank.fire = bulletsCount[tank.color];
                tank.fireReload = RELOAD_TICKS;
              }
            }
            break;
        }
        break;
    }
  }

  private _generateTanks() {
    const tanksPosition = [] as Position[];

    valuesIn(TankColor).forEach((color: TankColor, key: number) => {
      const tankContainer = GameInstance.addContainer();

      const randomDirection = Math.floor(
        Math.random() * valuesIn(Directions).length
      );
      const direction = valuesIn(Directions)[randomDirection];
      const sprite = imageManager.addTileSpriteToContainer(
        "mainSprite",
        tankContainer,
        {
          x: 0,
          y: 0,
        },
        {
          ...FRAMES[Directions.LEFT][AnimationType.IDLE][0],
          width: CELL_WIDTH,
          height: CELL_HEIGHT,
        }
      );

      const filter = new PIXI.filters.ColorMatrixFilter();
      tankContainer.filters = [filter];

      filter.hue(colorHue[color], true);
      //@ts-ignore
      const freeCell = GameInstance.stages.level.getNextAvilableCell(
        tanksPosition
      );
      tanksPosition.push(freeCell);
      const position = {
        x: freeCell.x * CELL_WIDTH,
        y: freeCell.y * CELL_HEIGHT,
      };
      this.tanks.push({
        color,
        position,
        direction,
        sprite,
        container: tankContainer,
        ticker: 0,
        speed: 0,
        rotationFrame: -1,
        rotationTickBlock: 0,
        prevDirection: direction,
        fireDelay: 0,
        fireReload: 0,
        fire: 0,
      });
      tankContainer.filters = [filter];
      tankContainer.position.set(position.x, position.y);
      if (key === this.activeTank) {
        GameInstance.changeViewTransition(position);
      }
    });

    const tankActiveContainer = GameInstance.addContainer();
    const activeSprite = imageManager.addTileSpriteToContainer(
      "activeIcon",
      tankActiveContainer,
      {
        x: 0,
        y: 0,
      },
      {
        x: 0,
        y: 0,
        width: ACTIVE_ICON_CELL_SIZE,
        height: ACTIVE_ICON_CELL_SIZE,
      }
    );
    this.activeIcon = {
      position: { x: 0, y: 0 },
      container: tankActiveContainer,
      sprite: activeSprite,
      ticker: 0,
    };
  }

  private _handleTankMove() {
    this.ticker = GameInstance.app.ticker.add((deltaTime: number) => {
      if (GameInstance.canPlay()) {
        const move = getVerticalInput() === 1;
        const rotation = getHorizontalInput();

        this.tanks.forEach((tank, key) => {
          if (key === this.activeTank) {
            if (move && tank.speed < MAX_SPEED) {
              tank.speed += ACCELERATION;
              if (tank.speed > MAX_SPEED) {
                tank.speed = MAX_SPEED;
              }
            }

            if (
              rotation &&
              tank.rotationFrame === -1 &&
              tank.rotationTickBlock === 0
            ) {
              tank.prevDirection = tank.direction;
              tank.rotationTickBlock = NEXT_ROTATION_DELAY;
              tank.direction = this._getNewDirectionByInput(
                tank.direction,
                rotation
              );
              tank.ticker = 0; //reset ticker to start new Animation
              tank.rotationFrame = 0;
            }
          }
          //tickers delay
          if (tank.rotationTickBlock > 0) {
            tank.rotationTickBlock--;
          }

          if (tank.rotationFrame > -1) {
            tank.rotationFrame += ANIMATION_SPEED_FRAME;
          }
          if (tank.fireDelay > 0) {
            tank.fireDelay--;
          }
          if (tank.fireReload > 0) {
            tank.fireReload--;
          }
          if (Math.floor(tank.rotationFrame) >= ROTATION_FRAMES) {
            tank.rotationFrame = -1;
            tank.prevDirection = tank.direction;
          }
          // - end-tickers change

          if (!move || key !== this.activeTank) {
            if (tank.speed > 0) {
              tank.speed -= SLOW_DOWN * deltaTime;
              if (tank.speed < 0) {
                tank.speed = 0;
              }
            }
          }

          if (tank.speed > 0) {
            GameInstance.changeViewTransition({
              x: tank.container.position.x,
              y: tank.container.position.y,
            });
          }
          if (tank.fire > 0 && tank.fireDelay === 0 && tank.fire > 0) {
            tank.fireDelay = FIRE_DELAY;
            tank.fire--;
            GameInstance.Emit(ActionType.FIRE, { tank });
          }

          const animationType = this._getAnimationType(tank);

          if (FRAMES[tank.direction] && FRAMES[tank.direction][animationType]) {
            let animationsFrames = FRAMES[tank.direction][animationType];
            const maxTick = animationsFrames.length - 1;
            let newTick = tank.ticker + ANIMATION_SPEED_FRAME;
            let tick = Math.floor(newTick);

            if (tick > maxTick) {
              newTick = 0;
              tick = 0;
            }
            tank.ticker = newTick;
            tank.sprite.tilePosition.set(
              animationsFrames[tick].x * CELL_WIDTH,
              animationsFrames[tick].y * CELL_HEIGHT
            );
          }
          this._changeTankPosition(tank, this.activeTank === key);
        });
      }
    });
  }

  _updateActiveIconTicker(position: Position) {
    let ticker = this.activeIcon.ticker + ANIMATION_SPEED_FRAME;
    if (ticker > ACTIVE_ICON_TICKER) {
      ticker = 0;
    }
    this.activeIcon.ticker = Math.floor(ticker);
    const posDiff = (ACTIVE_ICON_CELL_SIZE - CELL_WIDTH) / 2;

    this.activeIcon.container.position.set(
      position.x - posDiff,
      position.y - posDiff
    );
    this.activeIcon.sprite.tilePosition.set(
      ICON_FRAMES[ticker].x * ACTIVE_ICON_CELL_SIZE,
      ICON_FRAMES[ticker].y * ACTIVE_ICON_CELL_SIZE
    );
  }

  _getActiveTank() {
    return this.tanks[this.activeTank];
  }

  _changeTankPosition(tank: Tank, updateIcon: boolean) {
    let newPosition = {
      x: tank.container.position.x,
      y: tank.container.position.y,
    };

    switch (tank.direction) {
      case Directions.LEFT:
        newPosition = {
          x: tank.container.position.x - tank.speed,
          y: tank.container.position.y,
        };
        break;
      case Directions.RIGHT:
        newPosition = {
          x: tank.container.position.x + tank.speed,
          y: tank.container.position.y,
        };
        break;
      case Directions.TOP:
        newPosition = {
          x: tank.container.position.x,
          y: tank.container.position.y - tank.speed,
        };
        break;
      case Directions.BOTTOM:
        newPosition = {
          x: tank.container.position.x,
          y: tank.container.position.y + tank.speed,
        };
        break;
    }
    const findCollider = getCollider(newPosition);

    if (findCollider) {
      switch (tank.direction) {
        case Directions.LEFT:
          newPosition = {
            x: findCollider.x + findCollider.width,
            y: tank.container.position.y,
          };
          break;
        case Directions.RIGHT:
          newPosition = {
            x: findCollider.x - CELL_WIDTH,
            y: tank.container.position.y,
          };
          break;
        case Directions.TOP:
          newPosition = {
            x: tank.container.position.x,
            y: findCollider.y + findCollider.height,
          };
          break;
        case Directions.BOTTOM:
          newPosition = {
            x: tank.container.position.x,
            y: findCollider.y - CELL_HEIGHT,
          };
          break;
      }
    }
    tank.container.position.set(
      Math.round(newPosition.x),
      Math.round(newPosition.y)
    );

    if (updateIcon) {
      this._updateActiveIconTicker(newPosition);
    }
  }

  _getNewDirectionByInput(rotation: Directions, input: -1 | 1) {
    switch (rotation) {
      case Directions.TOP:
        return input < 0 ? Directions.LEFT : Directions.RIGHT;
      case Directions.LEFT:
        return input < 0 ? Directions.BOTTOM : Directions.TOP;
      case Directions.BOTTOM:
        return input < 0 ? Directions.RIGHT : Directions.LEFT;
      case Directions.RIGHT:
        return input < 0 ? Directions.TOP : Directions.BOTTOM;
    }
  }

  _getAnimationType(tank: Tank) {
    const { speed, direction, prevDirection } = tank;
    if (direction !== prevDirection) {
      if (
        (prevDirection === Directions.TOP && direction === Directions.RIGHT) ||
        (prevDirection === Directions.RIGHT &&
          direction === Directions.BOTTOM) ||
        (prevDirection === Directions.BOTTOM &&
          direction === Directions.LEFT) ||
        (prevDirection === Directions.LEFT && direction === Directions.TOP)
      ) {
        return AnimationType.ROTATION_CW;
      }
      return AnimationType.ROTATION_CCW;
    }
    return speed === 0 ? AnimationType.IDLE : AnimationType.MOVE;
  }
}
