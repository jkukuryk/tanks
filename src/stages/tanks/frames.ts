export enum AnimationType {
  IDLE = "idle",
  MOVE = "move",
  ROTATION_CW = "rotation_cw",
  ROTATION_CCW = "rotation_ccw",
}
export enum Directions {
  TOP = "top",
  LEFT = "left",
  BOTTOM = "bottom",
  RIGHT = "right",
}

export const FRAMES = {
  [Directions.LEFT]: {
    [AnimationType.IDLE]: [
      { x: -1, y: 0 },
      { x: -2, y: 0 },
      { x: -3, y: 0 },
      { x: -4, y: 0 },
    ],
    [AnimationType.MOVE]: [
      { x: -5, y: 0 },
      { x: -6, y: 0 },
      { x: -7, y: 0 },
    ],
    [AnimationType.ROTATION_CW]: [
      { x: -4, y: -5 },
      { x: -3, y: -5 },
      { x: -2, y: -5 },
    ],
    [AnimationType.ROTATION_CCW]: [
      { x: -5, y: -4 },
      { x: -6, y: -4 },
      { x: -7, y: -4 },
    ],
  },
  [Directions.TOP]: {
    [AnimationType.IDLE]: [
      { x: -1, y: -2 },
      { x: -2, y: -2 },
      { x: -3, y: -2 },
      { x: -4, y: -2 },
    ],
    [AnimationType.MOVE]: [
      { x: -5, y: -2 },
      { x: -6, y: -2 },
      { x: -7, y: -2 },
    ],
    [AnimationType.ROTATION_CW]: [
      { x: -7, y: -4 },
      { x: -6, y: -4 },
      { x: -5, y: -4 },
    ],
    [AnimationType.ROTATION_CCW]: [
      { x: -2, y: -4 },
      { x: -3, y: -4 },
      { x: -4, y: -4 },
    ],
  },
  [Directions.RIGHT]: {
    [AnimationType.IDLE]: [
      { x: -1, y: -1 },
      { x: -2, y: -1 },
      { x: -3, y: -1 },
      { x: -4, y: -1 },
    ],
    [AnimationType.MOVE]: [
      { x: -5, y: -1 },
      { x: -6, y: -1 },
      { x: -7, y: -1 },
    ],
    [AnimationType.ROTATION_CW]: [
      { x: -4, y: -4 },
      { x: -3, y: -4 },
      { x: -2, y: -4 },
    ],
    [AnimationType.ROTATION_CCW]: [
      { x: -5, y: -5 },
      { x: -6, y: -5 },
      { x: -7, y: -5 },
    ],
  },
  [Directions.BOTTOM]: {
    [AnimationType.IDLE]: [
      { x: -1, y: -3 },
      { x: -2, y: -3 },
      { x: -3, y: -3 },
      { x: -4, y: -3 },
    ],
    [AnimationType.MOVE]: [
      { x: -5, y: -3 },
      { x: -6, y: -3 },
      { x: -7, y: -3 },
    ],
    [AnimationType.ROTATION_CW]: [
      { x: -7, y: -5 },
      { x: -6, y: -5 },
      { x: -5, y: -5 },
    ],
    [AnimationType.ROTATION_CCW]: [
      { x: -2, y: -5 },
      { x: -3, y: -5 },
      { x: -4, y: -5 },
    ],
  },
};

export const ICON_FRAMES = [
  { x: 0, y: 0 },
  { x: 1, y: 0 },
  { x: 2, y: 0 },
  { x: 3, y: 0 },
  { x: 4, y: 0 },
  { x: 4, y: 0 },
  { x: 4, y: 0 },
  { x: 3, y: 0 },
  { x: 2, y: 0 },
  { x: 1, y: 0 },
];
