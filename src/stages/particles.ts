import * as PIXI from "pixi.js";

import { imageManager } from "src/classes/imageManager";

import smokeSprite from "assets/smokeSprite.png";
import hayDamageSprite from "assets/hayDamageSprite.png";
import { ActionType } from "src/constants/actionTypes";

import { GameInstance } from "src/classes/gameEngine";
import { Position } from "src/constants/globalTypes";
import { CELL_HEIGHT, CELL_WIDTH } from "src/constants/dimensions";
enum ParticleType {
  SMOKE = "smoke",
  HAY_DAMAGE = "hayDamage",
}
interface ParticleItem {
  frame: number;
  container: PIXI.Container;
  sprite: PIXI.TilingSprite;
  type: ParticleType;
}
const smokeFrames = [
  { x: 0, y: 0 },
  { x: -1, y: 0 },
  { x: 0, y: -1 },
  { x: -1, y: -1 },
];
const hayDamageFrames = [
  { x: 0, y: 0 },
  { x: -1, y: 0 },
  { x: -2, y: 0 },
];

export class Particles {
  particle: ParticleItem[];

  constructor() {
    imageManager.add("smokeSprite", smokeSprite);
    imageManager.add("hayDamageSprite", hayDamageSprite);
    this.Reset();
  }

  public Reset() {
    this.particle = [] as ParticleItem[];
  }

  Preload() {
    GameInstance.app.ticker.add(() => {
      this.particle.forEach((particle, key) => {
        const framesList =
          particle.type === ParticleType.SMOKE ? smokeFrames : hayDamageFrames;

        if (particle.frame >= framesList.length) {
          this.particle.splice(key, 1);
          particle.container.destroy();
        } else {
          const animationTile = framesList[particle.frame];
          particle.sprite.tilePosition.set(
            animationTile.x * CELL_WIDTH,
            animationTile.y * CELL_HEIGHT
          );
        }
        particle.frame++;
      });
    });
  }

  Action(action: ActionType, data: any) {
    switch (action) {
      case ActionType.ADD_SMOKE:
        const smokePosition = data.position as Position;
        this._addParticle(
          {
            x: Math.round(smokePosition.x - CELL_WIDTH / 2),
            y: Math.round(smokePosition.y - CELL_HEIGHT / 2),
          },
          ParticleType.SMOKE
        );
        break;
      case ActionType.ADD_HAY_DAMAGE:
        const hayPosition = data.position as Position;
        this._addParticle(
          {
            x: Math.round(hayPosition.x),
            y: Math.round(hayPosition.y),
          },
          ParticleType.HAY_DAMAGE
        );
        break;
    }
  }

  _addParticle(position: Position, type: ParticleType) {
    const particleContainer = GameInstance.addContainer();
    const { x, y } = position;
    particleContainer.position.set(x, y);

    const sprite = imageManager.addTileSpriteToContainer(
      type === ParticleType.SMOKE ? "smokeSprite" : "hayDamageSprite",
      particleContainer,
      {
        x: 0,
        y: 0,
      },
      {
        x: 0,
        y: 0,
        width: CELL_WIDTH,
        height: CELL_HEIGHT,
      }
    );

    const newParticle = {
      container: particleContainer,
      frame: 0,
      sprite,
      type,
    };

    this.particle.push(newParticle);
  }
}
