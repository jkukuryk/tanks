import { imageManager } from "classes/imageManager";
import splashScreen from "assets/splashScreen.png";

import { GameInstance } from "src/classes/gameEngine";
import { ActionType } from "src/constants/actionTypes";
import * as PIXI from "pixi.js";
const SPLASH_SCREEN_WIDTH = 546;
const SPLASH_SCREEN_HEIGHT = 314;

export class SplashScreen {
  splashContainer: PIXI.Container;
  opacity: number;
  show: boolean;

  constructor() {
    imageManager.add("splashScreen", splashScreen);
    this.splashContainer = new PIXI.Container();
    this.show = false;
    this.opacity = 0;
  }

  public Preload() {
    GameInstance.app.stage.addChild(this.splashContainer);
    imageManager.addSpriteToContainer("splashScreen", this.splashContainer);

    GameInstance.app.ticker.add(() => {
      if (this.show && this.opacity < 1) {
        this.opacity += 0.1;
      }
      if (!this.show && this.opacity > 0) {
        this.opacity -= 0.05;
      }
      this.splashContainer.alpha = this.opacity;
      if (this.opacity > 0) {
        const { width, height } = GameInstance.gameSize;
        this.splashContainer.position.set(
          Math.round(width / 2 - SPLASH_SCREEN_WIDTH / 2),
          Math.round(height / 2 - SPLASH_SCREEN_HEIGHT / 2)
        );
      }
    });
  }
  public Action(action: ActionType, data: any) {
    switch (action) {
      case ActionType.AFTER_LEVEL_BUILD:
        this.show = true;
        break;

      case ActionType.START_GAME_PLAY:
        this.show = false;
        break;
    }
  }
}
