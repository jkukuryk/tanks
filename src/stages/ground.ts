import { imageManager } from "classes/imageManager";
import mainSprite from "assets/tanksSprite.png";

import {
  CELL_HEIGHT,
  CELL_WIDTH,
  CELLS_HORIZONTAL,
  CELLS_VERTICAL,
} from "constants/dimensions";
import { GameInstance } from "src/classes/gameEngine";

const GROUND_CELLS = [
  { x: 0, y: 0 },
  { x: 0, y: 1 },
  { x: 0, y: 2 },
];
export class Ground {
  transitionX: number;
  transitionY: number;

  constructor() {
    imageManager.add("mainSprite", mainSprite);
    this.Reset();
  }

  public Reset() {
    this.transitionX = 0;
    this.transitionY = 0;
  }

  public Start() {
    const backGroundContainer = GameInstance.addContainer();

    for (let i = 0; i < CELLS_HORIZONTAL; i++) {
      for (let j = 0; j < CELLS_VERTICAL; j++) {
        const spritePosition =
          GROUND_CELLS[Math.floor(Math.random() * GROUND_CELLS.length)];
        imageManager.addTileSpriteToContainer(
          "mainSprite",
          backGroundContainer,
          {
            x: i * CELL_WIDTH,
            y: j * CELL_HEIGHT,
          },
          {
            ...spritePosition,
            width: CELL_WIDTH,
            height: CELL_HEIGHT,
          }
        );
      }
    }
  }
}
