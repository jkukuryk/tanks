/*Randomly generate 25 hays and 50 walls on the map area.
 *Life of hay is 100 health, you can destroy it by bullets, but you can NOT destroy the wall.
 *When hay is destroyed it should disappear.
 */
import * as PIXI from "pixi.js";

import { GameInstance } from "src/classes/gameEngine";
import { imageManager } from "src/classes/imageManager";
import { addCollider, Collider, removeCollider } from "src/classes/colliders";

import {
  CELLS_HORIZONTAL,
  CELLS_VERTICAL,
  CELL_HEIGHT,
  CELL_WIDTH,
  GAME_HEIGHT,
  GAME_WIDTH,
} from "src/constants/dimensions";
import { CellType, Position } from "src/constants/globalTypes";
import mainSprite from "assets/tanksSprite.png";
import { ActionType } from "src/constants/actionTypes";

const HAYS_COUNT = 25;
const WALLS_COUNT = 50;
const HAYS_HEALTH = 100;

const HAY_FRAMES = [
  { x: 0, y: -3 },
  { x: 0, y: -4 },
  { x: 0, y: -5 },
];
const WALL_FRAMES = { x: 1, y: 4 };

type LevelItem = {
  x: number;
  y: number;
  type: CellType;
  hp?: number;
  sprite: PIXI.TilingSprite;
  container: PIXI.Container;
  colliderId: string;
};

export class Level {
  levelItems: LevelItem[];

  constructor() {
    imageManager.add("mainSprite", mainSprite);
  }

  public Reset() {
    this.levelItems = [] as LevelItem[];
  }

  public Start() {
    this.levelItems = [] as LevelItem[];
    for (let i = 0; i < HAYS_COUNT; i++) {
      this._placeCell(CellType.HAY, HAYS_HEALTH);
    }
    for (let i = 0; i < WALLS_COUNT; i++) {
      this._placeCell(CellType.WALL);
    }
    this._addGameBoardCollider();
    GameInstance.Emit(ActionType.AFTER_LEVEL_BUILD, null);
  }

  public removeLevelItem(item: LevelItem) {
    this.levelItems = [
      ...this.levelItems.filter((cell) => cell.colliderId !== item.colliderId),
    ];
    item.container.destroy();
    removeCollider(item.colliderId);
    GameInstance.Emit(ActionType.HAY_DESTROYED);
  }

  public getNextAvilableCell(additionalCells?: Position[]) {
    let isFreeCell = false;
    let x = 0;
    let y = 0;
    while (!isFreeCell) {
      x = Math.floor(Math.random() * CELLS_VERTICAL);
      y = Math.floor(Math.random() * CELLS_HORIZONTAL);
      isFreeCell = !this.levelItems.find(
        (cell) => cell.x === x && cell.y === y
      );
      if (
        additionalCells &&
        !!additionalCells.find((cell) => cell.x === x && cell.y === y)
      ) {
        isFreeCell = false;
      }
    }
    return { x, y };
  }

  private _addGameBoardCollider() {
    addCollider({ x: -CELL_WIDTH, y: 0 }, CellType.GAME_BORDER, {
      height: GAME_HEIGHT,
      width: CELL_WIDTH,
    });
    addCollider({ x: 0, y: -CELL_HEIGHT }, CellType.GAME_BORDER, {
      height: CELL_HEIGHT,
      width: GAME_WIDTH,
    });

    addCollider({ x: 0, y: GAME_HEIGHT }, CellType.GAME_BORDER, {
      height: CELL_HEIGHT,
      width: GAME_WIDTH,
    });
    addCollider({ x: GAME_WIDTH, y: 0 }, CellType.GAME_BORDER, {
      height: GAME_HEIGHT,
      width: CELL_WIDTH,
    });
  }

  private _placeCell(type: CellType, hp?: number | null) {
    const newPosition = this.getNextAvilableCell();
    const container = GameInstance.addContainer();
    const spritePosition = type === CellType.HAY ? HAY_FRAMES[0] : WALL_FRAMES;
    const newRealPosition = {
      x: newPosition.x * CELL_WIDTH,
      y: newPosition.y * CELL_HEIGHT,
    };
    const sprite = imageManager.addTileSpriteToContainer(
      "mainSprite",
      container,
      newRealPosition,
      {
        ...spritePosition,
        width: CELL_WIDTH,
        height: CELL_HEIGHT,
      }
    );
    const colliderId = addCollider(newRealPosition, type, container);
    const newItem = { ...newPosition, type, hp, sprite, container, colliderId };
    this.levelItems.push(newItem);
  }

  _onHit(colliderId: string, power: number) {
    const levelItemIndex = this.levelItems.findIndex(
      (item) => item.colliderId === colliderId
    );

    if (levelItemIndex > -1) {
      const item = this.levelItems[levelItemIndex];
      if (item.type === CellType.HAY) {
        item.hp = item.hp - power;

        if (item.hp <= 0) {
          this.removeLevelItem(item);
        } else if (item.hp <= HAYS_HEALTH / 2) {
          item.sprite.tilePosition.set(
            HAY_FRAMES[2].x * CELL_WIDTH,
            HAY_FRAMES[2].y * CELL_HEIGHT
          );
        } else {
          item.sprite.tilePosition.set(
            HAY_FRAMES[1].x * CELL_WIDTH,
            HAY_FRAMES[1].y * CELL_HEIGHT
          );
        }
      }
    }
  }

  Action(action: ActionType, data: any) {
    switch (action) {
      case ActionType.HIT:
        const { id } = data.collider as Collider;
        this._onHit(id, data.power);
        break;
    }
  }
}
